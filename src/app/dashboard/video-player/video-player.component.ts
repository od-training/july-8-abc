import { Component, OnChanges, Input } from '@angular/core';
import { Video } from '../types';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnChanges {
  @Input() video: Video | undefined;
  url: SafeUrl | undefined;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnChanges() {
    if (this.video) {
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl(
        'https://www.youtube.com/embed/' + this.video.id
      );
    }
  }
}
