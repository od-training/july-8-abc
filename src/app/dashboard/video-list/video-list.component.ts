import { Component, OnInit, Input, Output } from '@angular/core';
import { Video } from '../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() videoList: Video[] = [];
  @Input() selectedVideo: Video | undefined;

  constructor() {}

  ngOnInit() {
    console.log(this.selectedVideo);
  }
}
