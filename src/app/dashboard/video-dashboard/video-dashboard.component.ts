import { Component, OnInit } from '@angular/core';
import { map, tap } from 'rxjs/operators';

import { Video } from '../types';
import { VideoDataService } from '../../video-data.service';
import { Observable, combineLatest } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  videos: Observable<Video[]>;
  selectedVideo: Observable<Video | undefined>;

  constructor(vds: VideoDataService, route: ActivatedRoute) {
    this.videos = vds.loadVideos();
    this.selectedVideo = combineLatest([
      this.videos,
      route.queryParamMap
    ]).pipe(
      map(([videos, queryParams]) => {
        if (videos.length > 0) {
          const selectedId = queryParams.get('id');
          const selectedVideo = videos.find(v => v.id === selectedId);
          return selectedVideo ? selectedVideo : videos[0];
        }
      })
    );
  }

  ngOnInit() {}
}
