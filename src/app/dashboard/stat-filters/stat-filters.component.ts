import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnInit {
  details: FormGroup;

  constructor(f: FormBuilder) {
    this.details = f.group({
      author: [''],
      numViews: [0]
    });
  }

  ngOnInit() {}

  submitTheForm() {
    console.log(this.details.value);
  }
}
